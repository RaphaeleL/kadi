# Security policy

To report a possible security vulnerability, please prefer contacting one of
the current
[maintainers](https://gitlab.com/iam-cms/kadi/-/blob/master/AUTHORS.md)
directly via email rather than creating an issue on GitLab.

Once a confirmed security vulnerability is fixed, a new release will be issued
as soon as possible. **Note that security fixes will currently only be released
for the latest version of Kadi4Mat**.
