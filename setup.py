# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from setuptools import find_namespace_packages
from setuptools import setup


def get_requirements(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), encoding="utf-8") as f:
        return [req.strip() for req in f.readlines()]


with open(os.path.join("kadi", "version.py"), encoding="utf-8") as f:
    exec(f.read())


with open("README.md", encoding="utf-8") as f:
    long_description = f.read()


setup(
    name="kadi",
    version=__version__,
    license="Apache-2.0",
    author="Karlsruhe Institute of Technology",
    description="Karlsruhe Data Infrastructure for Materials Science",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://kadi.iam.kit.edu",
    project_urls={
        "Source code": "https://gitlab.com/iam-cms/kadi",
        "Documentation": "https://kadi4mat.readthedocs.io/en/stable",
        "Changelog": "https://gitlab.com/iam-cms/kadi/-/blob/master/HISTORY.md",
    },
    packages=find_namespace_packages(include=["kadi", "kadi.*"]),
    include_package_data=True,
    python_requires=">=3.7,<3.11",
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: JavaScript",
        "Programming Language :: Python :: 3",
    ],
    install_requires=get_requirements("requirements.txt"),
    extras_require={
        "dev": get_requirements("requirements.dev.txt"),
    },
    entry_points={
        "console_scripts": [
            "kadi=kadi.cli.main:kadi",
        ],
        "kadi_plugins": [
            "influxdb=kadi.plugins.influxdb.plugin",
            "zenodo=kadi.plugins.zenodo.plugin",
            "triplestore=kadi.plugins.triplestore.plugin",
        ],
    },
)
