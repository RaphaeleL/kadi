# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import re
from urllib.parse import quote

import requests
from flask import Blueprint
from flask import flash
from flask import jsonify
from flask import render_template_string
from flask_wtf import FlaskForm
from rdflib import Graph
from wtforms import SelectField
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import DataRequired

from . import BASE_APACHE_JENA_URL
from . import BASE_KADI_URL
from . import HTML_CODE
from . import PLUGIN_NAME
from . import SPARQL_QUERY_GET_ALL
from . import SPARQL_QUERY_SIZE
from . import USER_TOKEN
from kadi.plugins import hookimpl


class QueryForm(FlaskForm):
    query_content = StringField("Query", validators=[DataRequired()])
    dataset_name = SelectField("Dataset Name", validators=[DataRequired()], choices=[])
    submit = SubmitField("Submit")


bp = Blueprint(PLUGIN_NAME, __name__, url_prefix=f"/{PLUGIN_NAME}")


def get_connection(dataset_name: str) -> Graph:
    """
    This function is used to get a connection to the triplestore
    @param dataset_name: The name of the dataset
    @return: The graph
    """
    return Graph().parse(create_fuseki_url(dataset_name))


def query(connection: Graph, query: str) -> list:
    """
    This function is used to query a triplestore
    @param connection: The connection to the triplestore
    @param query: The query
    @return: The result of the query
    """
    return connection.query(query)


def create_fuseki_url(dataset_name: str, postfix="") -> str:
    """
    This function is used to create a valid apache jenafuseki url
    @param dataset_name: The name of the dataset
    @param postfix: The postfix of the url
    """
    return f"{BASE_APACHE_JENA_URL}/{quote(dataset_name, safe='')}/{postfix}"


def get_dataset_name(resource_identifier: str) -> list:
    """
    This function is used to get the dataset name of a resource
    @param resource_identifier: The identifier of the resource
    @return: The name of the dataset
    """
    response = requests.get(BASE_APACHE_JENA_URL + "/$/datasets")
    connection = get_connection(dataset_name)
    result = []
    if response.status_code == 200:
        datasets = response.json()
        for dataset in datasets["datasets"]:
            dataset_name = dataset["ds.name"].split("/")[1]
            query = SPARQL_QUERY_GET_ALL
            for row in query(connection, query):
                if (
                    resource_identifier in str(row[0])
                    or resource_identifier in str(row[1])
                    or resource_identifier in str(row[2])
                ):
                    result.append(dataset_name)
    connection.close()
    return result


@bp.get("/get/all/<dataset_name>")
def get_all(dataset_name: str) -> dict:
    """
    This function is used to get all the data from a dataset
    @param dataset_name: The name of the dataset
    @return: A JSON object containing the data
    """
    connection = get_connection(dataset_name)
    result = []
    for row in query(connection, SPARQL_QUERY_GET_ALL):
        result.append(
            {"subject": str(row[0]), "predicate": str(row[1]), "object": str(row[2])}
        )
    connection.close()
    return jsonify(result)


@bp.get("/upload/<record_id>")
def upload_data(record_id: int, dataset_name: str = "") -> dict:
    """
    This function is used to upload data to a dataset.
    @param record_id: The id of the record
    @param dataset_name: The name of the existing dataset
    @return: A JSON object containing the status code and the RDF data
    """
    headers = {
        "Authorization": f"Bearer {USER_TOKEN}",
        "Content-Type": "multipart/form-data",
    }
    url = f"{BASE_KADI_URL}/api/records/{record_id}/export/rdf"
    response = requests.get(url, headers=headers)
    content = response.content if response.status_code == 200 else ""
    pattern = r'schema:identifier "(.*?)" ;'
    if type(content) == bytes:
        match = re.search(pattern, content.decode("utf-8"))
        dataset_name = match.group(1) if match else str(record_id)
    else:
        dataset_name = dataset_name
    url = create_fuseki_url(dataset_name, "data")
    headers = {"Content-Type": "text/turtle"}
    if dataset_name not in get_all_datasets_helper():
        create_dataset(dataset_name)
    else:
        clear_data(dataset_name)
    response = requests.post(url, headers=headers, data=content)
    return jsonify({"statuscode": str(response.status_code)})


@bp.get("/clear/<dataset_name>")
def clear_data(dataset_name: str) -> dict:
    """
    This function is used to clear all the data from a dataset
    @param dataset_name: The name of the dataset
    @return: A JSON object containing the status code
    """
    url = create_fuseki_url(dataset_name, "update")
    response = requests.post(url, data="CLEAR ALL")
    return jsonify({"statuscode": str(response.status_code)})


@bp.get("/datasets/size/<dataset_name>")
def get_size(dataset_name: str) -> dict:
    """
    This function is used to get the size of a dataset
    @param dataset_name: The name of the dataset
    @return: A JSON object containing the size
    """
    connection = get_connection(dataset_name)
    result = query(connection, SPARQL_QUERY_SIZE)
    count = result.bindings[0]["count"].value
    connection.close()
    return jsonify({"size": str(count)})


def get_all_datasets_helper() -> list:
    """
    This function is used to get all the datasets
    @return: A list containing the datasets
    """
    response = requests.get(BASE_APACHE_JENA_URL + "/$/datasets")
    if response.status_code == 200:
        datasets, result = response.json(), []
        for dataset in datasets["datasets"]:
            dataset_name = dataset["ds.name"]
            result.append(dataset_name)
    return result


@bp.get("/datasets/get")
def get_all_datasets() -> dict:
    """
    This function is used to get all the datasets
    @return: A JSON object containing the datasets
    """
    return jsonify(get_all_datasets_helper())


@bp.get("/dataset/new/<dataset_name>")
def create_dataset(dataset_name: str) -> dict:
    """
    This function is used to create a new dataset
    @param dataset_name: The name of the dataset
    @return: A JSON object containing the status code
    """
    url = f"{BASE_APACHE_JENA_URL}/$/datasets"
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    data = f"dbName={dataset_name}&dbType=tdb2&state=active"
    response = requests.post(url, headers=headers, data=data)
    return jsonify({"statuscode": str(response.status_code)})


@bp.get("/dataset/remove/<dataset_name>")
def remove_dataset(dataset_name: str) -> dict:
    """
    This function is used to remove a dataset
    @param dataset_name: The name of the dataset
    @return: A JSON object containing the status code
    """
    url = f"{BASE_APACHE_JENA_URL}/$/datasets/{quote(dataset_name, safe='')}"
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    response = requests.delete(url, headers=headers)
    return jsonify({"statuscode": str(response.status_code)})


@bp.route("/playground", methods=["GET", "POST"])
def playground():
    """
    This function is used to query a dataset
    @return: A HTML page containing the query form and the query result
    """
    form, result = QueryForm(), []
    for choice in get_all_datasets_helper():
        form.dataset_name.choices.append((choice, choice[1:]))
    if form.validate_on_submit():
        connection = get_connection(form.dataset_name.data[1:])
        try:
            for line in form.query_content.data.upper().strip().split("\n"):
                if line.strip().split(" ")[0] in ["INSERT", "DELETE", "DROP", "CREATE"]:
                    flash("This is a read-only playground.", "error")
                    return render_template_string(
                        HTML_CODE, form=form, query_result=result
                    )
                elif line.strip().split(" ")[0] in ["SELECT", "PREFIX"]:
                    pass
            for row in query(connection, form.query_content.data):
                result_string = ""
                for item in row:
                    result_string += f"{item}\n"
                result.append(result_string)
        except Exception as e:
            err = e.__class__.__name__
            message = "Incorrect Query" if err == "ParseException" else "Parse Error"
            flash(message, "error")
            return render_template_string(HTML_CODE, form=form, query_result=result)
        finally:
            connection.close()
    return render_template_string(HTML_CODE, form=form, query_result=result)


@hookimpl
def kadi_get_blueprints():
    return bp


@hookimpl
def kadi_post_resource_change(resource, user, created):
    for dataset_name in get_dataset_name(resource.identifier):
        # Workes well, but 'elasticsearch.exceptions.UnsupportedProductError' is thrown.
        # But i don't use elasticsearch, so ..
        clear_data(dataset_name)
        upload_data(resource.id, dataset_name)
