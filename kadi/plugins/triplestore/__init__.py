# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
USER_TOKEN = "pat_8afa8945601ef3ad46232157471cafc36badf969641ff594"  # user/user Demo Account Token
BASE_APACHE_JENA_URL = "http://localhost:3030"
BASE_KADI_URL = "http://localhost:5000"
PLUGIN_NAME = "triplestore"
SPARQL_QUERY_GET_ALL = """
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT * WHERE { ?sub ?pred ?obj .  }
    ORDER BY DESC(?sub)
"""
SPARQL_QUERY_SIZE = """
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT (COUNT(*) AS ?count)
    WHERE { ?s ?p ?o }
"""
HTML_CODE = """
    <!DOCTYPE html>
    <html>
        <head>
            <title>Read-Only Playground</title>
            <style>
                * {
                    box-sizing: border-box;
                }
                body {
                    font-family: Arial, sans-serif;
                    margin: 0;
                    padding: 0;
                    background-color: #f0f0f0;
                }
                .form-container, .result-container {
                    width: 80%;
                    margin: 0 auto;
                    padding: 20px;
                    background-color: #fff;
                    border-radius: 5px;
                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                }
                h1, h2 {
                    text-align: center;
                    color: #333;
                }
                textarea, select {
                    width: 100%;
                    padding: 10px;
                    border: 1px solid #ddd;
                    border-radius: 5px;
                    font-size: 16px;
                    margin-bottom: 10px;
                }
                .submit-btn {
                    display: block;
                    width: 100%;
                    padding: 10px;
                    background-color: #008000;
                    color: #fff;
                    border: none;
                    border-radius: 5px;
                    cursor: pointer;
                    font-size: 20px; /* Increase font size to make the arrow more visible */
                }
                .submit-btn:hover {
                    background-color: #006400;
                }
                pre {
                    background-color: #f8f8f8;
                    border: 1px solid #ddd;
                    padding: 10px;
                    border-radius: 5px;
                }
                .alert {
                    display: block;
                    width: 100%;
                    padding: 10px;
                    border: none;
                    border-radius: 5px;
                }
                .alert-error {
                    background-color: #ff3131;
                    color: #fff;
                }
            </style>
            </head>
            <body>
                <h1>Apache Jena Fuseki RDF Triplestore</h1>
                <h2>Read-Only Playground</h2>
                <div class="form-container">
                    {% with messages = get_flashed_messages(with_categories=true) %}
                        {% if messages %}
                            {% for category, message in messages %}
                                <div class="alert alert-{{ category }}">{{ message }}</div>
                            {% endfor %}
                        {% endif %}
                    {% endwith %}
                    <br>
                    <form method="post">
                        {{ form.csrf_token }}
                        {{ form.hidden_tag() }}
                        <textarea class="query-input" name="query_content" rows="5" placeholder="Enter your query here">{{ form.query_content.data if form.query_content.data is not none }}</textarea>
                        <br>
                        <select class="dataset-name" name="dataset_name">
                            {% for choice in form.dataset_name.choices %}
                                <option value="{{ choice[0] }}" {% if choice[0] == form.dataset_name.data %}selected{% endif %}>
                                    {{ choice[1] }}
                                </option>
                            {% endfor %}
                        </select>
                        <br>
                        <button class="submit-btn" type="submit">▶ Run</button> <!-- Add a play arrow before 'Run' -->
                    </form>
                </div>
                {% if query_result %}
                    <h2>Result</h2>
                    <div class="result-container">
                        {% for result in query_result %}
                            <pre>{{ result }}</pre>
                        {% endfor %}
                    </div>
                {% endif %}
                <br><br>
            </body>
    </html>
"""
