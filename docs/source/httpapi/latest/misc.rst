.. _httpapi-latest-misc:

Miscellaneous
=============

.. include:: ../snippets/misc.rst

GET
---

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.main.api
    :methods: get
    :autoquickref:
