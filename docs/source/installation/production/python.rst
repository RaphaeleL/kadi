.. _installation-production-python:

.. include:: ../snippets/python.rst

.. _installation-production-python-updating:

Updating existing installation
------------------------------

When updating Python in existing installations, the existing virtual environment where
Kadi is installed needs to be recreated, e.g. when using a structure as described in the
installation instructions:

.. code-block:: bash

    sudo su - kadi                   # Switch to the kadi user
    rm -rf ${HOME}/venv              # Remove the old virtual environment
    python3.x -m venv ${HOME}/venv   # Create the new virtual environment
    source ${HOME}/venv/bin/activate # Activate the new virtual environment
    pip install wheel                # Install the wheel package
    pip install -U pip               # Make sure the newest version of pip is being used
    pip install kadi                 # Install Kadi again

When using server configuration files such as described in the installation instructions
for Apache and/or uWSGI, some paths defined in these files referring to the installed
Kadi Python package may need to be updated accordingly. An easy way to do this is to
regenerate these files using the Kadi CLI:

.. code-block:: bash

    sudo su - kadi    # Switch to the kadi user
    kadi utils apache # Generate and print a basic configuration for Apache
    kadi utils uwsgi  # Generate and print a basic configuration for uWSGI

The generated content can then be compared with the corresponding configuration files in
``/etc/apache2/sites-available/kadi.conf`` and ``/etc/kadi-uwsgi.ini`` (when using the
structure as described in the installation instructions) to update them accordingly.
