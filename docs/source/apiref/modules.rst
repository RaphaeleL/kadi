.. _apiref-modules:

Modules
=======

This section contains a complete API reference of the :mod:`kadi.modules` Python module.

Accounts
--------

.. automodule:: kadi.modules.accounts.core
    :members:

.. automodule:: kadi.modules.accounts.forms
    :members:

.. automodule:: kadi.modules.accounts.models
    :members:
    :exclude-members: query

.. automodule:: kadi.modules.accounts.providers.core
    :members:

.. automodule:: kadi.modules.accounts.providers.ldap
    :members:

.. automodule:: kadi.modules.accounts.providers.local
    :members:

.. automodule:: kadi.modules.accounts.providers.shib
    :members:

.. automodule:: kadi.modules.accounts.schemas
    :members:

.. automodule:: kadi.modules.accounts.utils
    :members:

Collections
-----------

.. automodule:: kadi.modules.collections.core
    :members:

.. automodule:: kadi.modules.collections.export
    :members:

.. automodule:: kadi.modules.collections.forms
    :members:

.. automodule:: kadi.modules.collections.mappings
    :members:

.. automodule:: kadi.modules.collections.models
    :members:
    :exclude-members: query

.. automodule:: kadi.modules.collections.schemas
    :members:

.. automodule:: kadi.modules.collections.utils
    :members:

Groups
------

.. automodule:: kadi.modules.groups.core
    :members:

.. automodule:: kadi.modules.groups.forms
    :members:

.. automodule:: kadi.modules.groups.mappings
    :members:

.. automodule:: kadi.modules.groups.models
    :members:
    :exclude-members: query

.. automodule:: kadi.modules.groups.schemas
    :members:

.. automodule:: kadi.modules.groups.utils
    :members:

Main
----

.. automodule:: kadi.modules.main.forms
    :members:

.. automodule:: kadi.modules.main.tasks
    :members:

.. automodule:: kadi.modules.main.utils
    :members:

Records
-------

.. automodule:: kadi.modules.records.core
    :members:

.. automodule:: kadi.modules.records.export
    :members:

.. automodule:: kadi.modules.records.extras
    :members:

.. automodule:: kadi.modules.records.files
    :members:

.. automodule:: kadi.modules.records.forms
    :members:

.. automodule:: kadi.modules.records.links
    :members:

.. automodule:: kadi.modules.records.mappings
    :members:

.. automodule:: kadi.modules.records.models
    :members:
    :exclude-members: query

.. automodule:: kadi.modules.records.previews
    :members:

.. automodule:: kadi.modules.records.schemas
    :members:

.. automodule:: kadi.modules.records.streaming_uploads
    :members:

.. automodule:: kadi.modules.records.tasks
    :members:

.. automodule:: kadi.modules.records.uploads
    :members:

.. automodule:: kadi.modules.records.utils
    :members:

Settings
--------

.. automodule:: kadi.modules.settings.forms
    :members:

.. automodule:: kadi.modules.settings.utils
    :members:

Sysadmin
--------

.. automodule:: kadi.modules.sysadmin.forms
    :members:

.. automodule:: kadi.modules.sysadmin.utils
    :members:

Templates
---------

.. automodule:: kadi.modules.templates.core
    :members:

.. automodule:: kadi.modules.templates.export
    :members:

.. automodule:: kadi.modules.templates.forms
    :members:

.. automodule:: kadi.modules.templates.mappings
    :members:

.. automodule:: kadi.modules.templates.models
    :members:
    :exclude-members: query

.. automodule:: kadi.modules.templates.schemas
    :members:

.. automodule:: kadi.modules.templates.utils
    :members:
