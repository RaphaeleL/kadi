.. _development-plugins:

Plugins
=======

This section describes how plugins can be developed to extend the functionality of
|kadi| without having to touch the main application code. As currently all first party
plugins are part of the main application package, all existing examples can be found in
the :mod:`kadi.plugins` module. This also includes code not implemented as a separate
plugin, but still using the plugin hooks.

Plugin infrastructure
---------------------

The plugin infrastructure in |kadi| builds upon `pluggy
<https://pluggy.readthedocs.io/en/stable>`__, a simple and flexible plugin system. It
allows implementing different plugin interface functions, called hook specifications,
which a plugin can choose to implement. Each hook will be called in certain places in
the application flow, which will invoke all corresponding implementations of that hook.

In order for the main application to find a plugin, it has to register itself using the
``kadi_plugins`` setuptools entry point, as also done for all first party plugins in
``setup.py``. Each plugin needs to specify a unique name and the module that contains
all hook implementations:

.. code-block:: python3

    entry_points={
        "kadi_plugins": [
            "example=kadi_plugin_example.plugin",
        ],
    },

In this case, the plugin is called ``example``, while the hook implementations are
loaded via the :mod:`kadi_plugin_example.plugin` module of some package called
``kadi_plugin_example``.

.. note::
    Whenever changing any content related to entry points, it is necessary to reinstall
    the application in order to fully propagate these changes:

    .. code-block:: bash

        pip install -e .[dev]

Plugin hooks
------------

:ref:`Plugin hooks <apiref-plugins-hooks>` can be implemented by using the ``hookimpl``
decorator. The name of the decorated function must correspond to the respective plugin
hook. Plugin configurations, specified via the ``PLUGIN_CONFIG`` configuration value,
can be loaded by using the ``get_plugin_config`` function (see also how to :ref:`install
plugins <installation-plugins>`). An example implementation could look like the
following:

.. code-block:: python3

    from kadi.plugins import get_plugin_config
    from kadi.plugins import hookimpl

    @hookimpl
    def kadi_example_plugin_hook():
        # Load the configuration of the "example" plugin, if necessary.
        config = get_plugin_config("example")
        # Validate the configuration or do something else with it.

To see a list of all currently existing hook specifications, please refer to the
:ref:`API reference <apiref-plugins-hooks>`.

.. warning::
    As long as |kadi| is still in beta, hook specifications are still subject to change
    without prior deprecation notice.

Plugin API
----------

As plugins might need to make use of some of |kadi|'s internal functionality in order to
properly implement any hooks, a subset of the most relevant functions, classes and
modules that a plugin might want to use are available via the :mod:`kadi.plugins`
module, as also shown in the example above and listed in ``kadi/plugins/__init__.py``.
This module defines the public API that third party plugins may use without fear of
unexpected breaking changes.

.. warning::
    As long as |kadi| is still in beta, the public plugin API is still subject to change
    without prior deprecation notice.
