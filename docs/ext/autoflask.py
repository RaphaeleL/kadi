# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import itertools
import re
from collections import OrderedDict

from docutils.parsers.rst import directives
from flask import json
from marshmallow import fields
from marshmallow import missing
from sphinxcontrib.autohttp.common import import_object
from sphinxcontrib.autohttp.flask import AutoflaskDirective
from sphinxcontrib.autohttp.flask_base import AutoflaskBase as _AutoflaskBase
from sphinxcontrib.autohttp.flask_base import get_routes
from sphinxcontrib.autohttp.flask_base import prepare_docstring
from werkzeug.http import HTTP_STATUS_CODES

import kadi.lib.constants as const
from kadi.lib.schemas import FilteredString


# The following code is a slightly modified version of httpdomain's autoflask directive
# and corresponding utility functions, which are licensed under the two-clause BSD
# license and available at the following URL:
# https://github.com/sphinx-contrib/httpdomain/tree/1.8.1/sphinxcontrib/autohttp
#
# Copyright (c) 2010 by the contributors (see AUTHORS file).
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# pylint: disable=missing-class-docstring, missing-function-docstring


def _reqform(form_data):
    for name, meta in form_data:
        line = f"    * **{name}**"

        if "required" in meta:
            line += " (*Required*)"

        line += f" - {meta.get('type', 'String')}"

        if "default" in meta:
            line += f" (*Default:* ``{meta['default']}``)"

        yield line


def _reqschema(fields, indent=4):
    for name, meta in fields.items():
        line = f"{indent * ' '}* **{name}**"

        if meta.get("required", False):
            line += " (*Required*)"

        if meta.get("many", False):
            line += f" - [{meta['type']}]"
        else:
            line += f" - {meta['type']}"

        if meta.get("default", missing) != missing:
            line += f" (*Default:* ``{json.dumps(meta['default'])}``)"

        yield line

        if "nested" in meta:
            yield ""
            yield from _reqschema(meta["nested"], indent + 2)
            yield ""


def _get_reqschema_fields(schema):
    type_mapping = {
        FilteredString: "String",
        fields.String: "String",
        fields.Integer: "Integer",
        fields.Float: "Float",
        fields.Boolean: "Boolean",
        fields.DateTime: "Datetime",
        fields.Raw: "Any",
    }
    type_fallback = "Object"

    fields_meta = OrderedDict()

    for name, field in schema.fields.items():
        if not field.dump_only:
            field_meta = {
                "type": type_mapping.get(field.__class__, type_fallback),
                "required": field.required,
                "default": field.load_default,
            }

            if schema.partial is True or (
                isinstance(schema.partial, tuple) and name in schema.partial
            ):
                field_meta["required"] = False

            if isinstance(field, fields.Pluck):
                field_meta["many"] = field.many
                field_meta["type"] = type_mapping.get(
                    field.schema.fields[field.field_name].__class__, type_fallback
                )

            elif isinstance(field, fields.Nested):
                field_meta["many"] = field.many
                field_meta["nested"] = _get_reqschema_fields(field.schema)
                # For nested types, we only care about the default value in the
                # innermost schema.
                field_meta["default"] = missing

            fields_meta[name] = field_meta

    sorted_by_name = sorted(fields_meta.items(), key=lambda field: field[0])

    return OrderedDict(
        sorted(sorted_by_name, key=lambda field: field[1]["required"], reverse=True)
    )


def http_directive(method, paths, docstring, apidoc_meta):
    # Method and endpoint.
    method = method.lower().strip()
    paths = [paths] if isinstance(paths, str) else paths

    for path in paths:
        yield f".. http:{method}:: {path}"

    # Docstring.
    if isinstance(docstring, str):
        docstring = docstring.splitlines()

    yield ""
    for line in docstring:
        yield f"  {line}"

    # Versions.
    versions = apidoc_meta.get(const.APIDOC_VERSIONS_KEY, const.API_VERSIONS)

    if versions:
        yield ""
        versions = [
            f"``{v}``" + (" (*latest*)" if v == const.API_VERSIONS[-1] else "")
            for v in versions
        ]
        versions.reverse()
        yield f"  **Versions:** {', '.join(versions)}"

    # Required scopes.
    if const.APIDOC_SCOPES_KEY in apidoc_meta:
        scopes = apidoc_meta[const.APIDOC_SCOPES_KEY]["scopes"]
        operator = apidoc_meta[const.APIDOC_SCOPES_KEY]["operator"]

        required_scopes = f"`` *{operator}* ``".join(scopes)

        yield ""
        yield f"  **Required scopes:** ``{required_scopes}``"

    # Query parameters.
    if (
        const.APIDOC_QPARAMS_KEY in apidoc_meta
        or const.APIDOC_PAGINATION_KEY in apidoc_meta
    ):
        yield ""
        yield "  **Query parameters**"
        yield ""

        # Pagination.
        pagination_meta = apidoc_meta.get(const.APIDOC_PAGINATION_KEY)

        if pagination_meta:
            page = "    * **page** - The current result page"

            if pagination_meta["page_max"]:
                page += f", limited to a maximum of ``{pagination_meta['page_max']}``."
            else:
                page += "."

            page += " (*Default:* ``1``)"
            yield page

            per_page = "    * **per_page** - Number of results per page"

            if pagination_meta["per_page_max"]:
                per_page += (
                    f", limited to a maximum of ``{pagination_meta['per_page_max']}``."
                )
            else:
                page += "."

            per_page += " (*Default:* ``10``)"
            yield per_page

        # Others.
        for name, qparam_meta in apidoc_meta.get(const.APIDOC_QPARAMS_KEY, {}).items():
            qparam = f"    * **{name}** - {qparam_meta['description']}"

            if qparam_meta["multiple"]:
                qparam += " (*Can be specified more than once.*)"
            else:
                default = qparam_meta["default"]

                # Ignore None values and empty strings.
                if default is not None and default != "":
                    qparam += f" (*Default:* ``{default}``)"

            yield qparam

    # JSON request body via a marshmallow schema.
    if const.APIDOC_REQ_SCHEMA_KEY in apidoc_meta:
        reqschema_meta = apidoc_meta[const.APIDOC_REQ_SCHEMA_KEY]

        yield ""
        yield "  **Request JSON object**"
        yield ""

        if reqschema_meta["description"]:
            yield f"    {reqschema_meta['description']}"
            yield ""

        fields = _get_reqschema_fields(reqschema_meta["schema"])
        yield from _reqschema(fields)

    # Form request body.
    if const.APIDOC_REQ_FORM_KEY in apidoc_meta:
        reqform_meta = apidoc_meta[const.APIDOC_REQ_FORM_KEY]

        yield ""
        yield f"  **Request form data** (*Encoding:* ``{reqform_meta['enctype']}``)"
        yield ""

        if reqform_meta["description"]:
            yield f"    {reqform_meta['description']}"
            yield ""

        yield from _reqform(reqform_meta["form_data"])

    # Status codes.
    if const.APIDOC_STATUS_CODES_KEY in apidoc_meta:
        yield ""
        yield "  **Status codes**"
        yield ""

        for status, description in apidoc_meta[const.APIDOC_STATUS_CODES_KEY].items():
            yield (
                f"    * **{status}** (*{HTTP_STATUS_CODES.get(status, 'Unknown')}*)"
                f" - {description}"
            )

    yield ""


def quickref_directive(method, path, docstring):
    method = method.lower().strip()

    if isinstance(docstring, str):
        docstring = docstring.splitlines()

    if len(docstring) > 0:
        description = docstring[0]
    else:
        description = ""

    ref = path.replace("<", "(").replace(">", ")").replace("/", "-").replace(":", "-")

    yield f"    * - `{method.upper()} {path} <#{method.lower()}-{ref}>`_"
    yield f"      - {description}"


class AutoflaskBase(_AutoflaskBase):
    # pylint: disable=abstract-method

    @property
    def version(self):
        return self.options.get("version")

    @property
    def packages(self):
        packages = self.options.get("packages")

        if not packages:
            return None

        return re.split(r"\s*,\s*", packages)

    @property
    def methods(self):
        methods = self.options.get("methods")

        if not methods:
            return None

        return [m.lower() for m in re.split(r"\s*,\s*", methods)]

    def get_routes_iter(self, app):
        routes = self.inspect_routes(app)

        if "view" in self.groupby:
            routes = self.groupby_view(routes)

        return routes

    def make_rst(self, qref=False):
        app = import_object(self.arguments[0])
        autoquickref = self.options.get("autoquickref", False) is None

        if autoquickref:
            yield ""
            yield ".. list-table::"
            yield "    :class: narrow-table"
            yield ""

            routes_iter = self.get_routes_iter(app)

            for method, paths, view_func, view_doc in routes_iter:
                docstring = prepare_docstring(view_doc)

                for path in paths:
                    yield from quickref_directive(method, path, docstring)

        routes_iter = self.get_routes_iter(app)

        for method, paths, view_func, view_doc in routes_iter:
            docstring = prepare_docstring(view_doc)
            apidoc_meta = getattr(view_func, const.APIDOC_META_ATTR, {})

            yield from http_directive(method, paths, docstring, apidoc_meta)

    def inspect_routes(self, app):
        order = self.order or "path"

        if self.endpoints:
            routes = itertools.chain(
                *(get_routes(app, endpoint, order) for endpoint in self.endpoints)
            )
        else:
            routes = get_routes(app, order=order)

        for method, paths, endpoint in routes:
            try:
                blueprint, _, _ = endpoint.rpartition(".")

                if (
                    self.blueprints and blueprint not in self.blueprints
                ) or blueprint in self.undoc_blueprints:
                    continue

            except ValueError:
                pass

            if endpoint == "static" or endpoint in self.undoc_endpoints:
                continue

            view = app.view_functions[endpoint]

            if (self.modules and view.__module__ not in self.modules) or (
                self.undoc_modules and view.__module__ in self.modules
            ):
                continue

            view_class = getattr(view, "view_class", None)

            if view_class is None:
                view_func = view
            else:
                view_func = getattr(view_class, method.lower(), None)

            # Filter out internal and experimental endpoints.
            apidoc_meta = getattr(view_func, const.APIDOC_META_ATTR, {})

            if apidoc_meta.get(const.APIDOC_EXPERIMENTAL_KEY, False) or apidoc_meta.get(
                const.APIDOC_INTERNAL_KEY, False
            ):
                continue

            # Filter out endpoints not being in the specified packages.
            skip_endpoint = True

            if self.packages:
                for package in self.packages:
                    if view.__module__.startswith(package):
                        skip_endpoint = False
                        break

                if skip_endpoint:
                    continue

            # Filter out endpoints not having the correct HTTP methods.
            if self.methods and method.lower() not in self.methods:
                continue

            # Filter out endpoints not belonging to the given API version.
            if self.version:
                if not re.match(f"^.*_{self.version}$", endpoint):
                    continue
            # Otherwise, skip all versioned endpoints.
            else:
                if re.match("^.*_v[0-9]+$", endpoint):
                    continue

            view_doc = view.__doc__ or ""

            if view_func and view_func.__doc__:
                view_doc = view_func.__doc__

            if not view_doc and "include-empty-docstring" not in self.options:
                continue

            yield (method, paths, view_func, view_doc)


class Autoflask(AutoflaskBase, AutoflaskDirective):
    """Modified ``autoflask`` directive.

    Has some modified output, default values and filters and additionally provides the
    following options:

    * ``:version:`` Specify a single API version that should be documented (based on the
      associated endpoints). If the option is not provided, all unversioned endpoints
      (which automatically point to the latest version) will be taken instead.
    * ``:packages:`` Limits the documented endpoints to those in the specified packages
      and its subpackages. Multiple packages can be given separated by commas.
    * ``:methods:`` Limits the documented endpoints by specified HTTP methods. Multiple
      methods can be given separated by commas.

    **Example:**

    .. code-block:: rst

        .. autoflask:: kadi.wsgi:app
            :version: v1
            :packages: kadi.modules.records.api
            :methods: get, post
    """


Autoflask.option_spec["version"] = directives.unchanged
Autoflask.option_spec["packages"] = directives.unchanged
Autoflask.option_spec["methods"] = directives.unchanged


def setup(app):
    app.add_directive("autoflask", Autoflask)
