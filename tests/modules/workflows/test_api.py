# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_select_workflow_tools(client, user_session):
    """Test the internal "api.select_workflow_tools" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_workflow_tools"))
        check_api_response(response)


@pytest.mark.parametrize("endpoint", ["get_workflows", "get_workflow_tools"])
def test_get_workflow_endpoints(
    endpoint, api_client, dummy_personal_token, dummy_record
):
    """Test the remaining "api.get_workflow*" endpoints."""
    response = api_client(dummy_personal_token).get(url_for(f"api.{endpoint}"))
    check_api_response(response)
