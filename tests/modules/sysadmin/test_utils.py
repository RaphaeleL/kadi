# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

import kadi.lib.constants as const
from kadi.lib.config.core import get_sys_config
from kadi.lib.config.core import MISSING
from kadi.lib.config.core import remove_sys_config
from kadi.lib.config.core import set_sys_config
from kadi.lib.config.models import ConfigItem
from kadi.lib.storage.misc import create_misc_storage
from kadi.lib.web import url_for
from kadi.modules.sysadmin.utils import delete_index_image
from kadi.modules.sysadmin.utils import get_legals_modification_date
from kadi.modules.sysadmin.utils import legals_acceptance_required
from kadi.modules.sysadmin.utils import save_index_image
from tests.utils import check_view_response


def test_sysadmin_required(client, dummy_user, user_session):
    """Test if the "sysadmin_required" decorator works correctly."""
    endpoint = url_for("sysadmin.view_information")

    with user_session():
        # Unpermitted request.
        response = client.get(endpoint)
        check_view_response(response, status_code=404)

        # Permitted request.
        dummy_user.is_sysadmin = True

        response = client.get(endpoint)
        check_view_response(response)


def test_save_index_image(dummy_image):
    """Test if saving the index image works correctly."""
    storage = create_misc_storage()

    save_index_image(dummy_image)
    first_image_identifier = get_sys_config(
        const.SYS_CONFIG_INDEX_IMAGE, use_fallback=False
    )

    assert first_image_identifier is not MISSING
    assert storage.exists(storage.create_filepath(first_image_identifier))

    dummy_image.seek(0)
    save_index_image(dummy_image)
    second_image_identifier = get_sys_config(
        const.SYS_CONFIG_INDEX_IMAGE, use_fallback=False
    )

    assert second_image_identifier is not MISSING
    assert not storage.exists(storage.create_filepath(first_image_identifier))
    assert storage.exists(storage.create_filepath(second_image_identifier))


def test_delete_index_image(dummy_image):
    """Test if deleting the index image works correctly."""
    storage = create_misc_storage()

    save_index_image(dummy_image)
    delete_index_image()

    assert not ConfigItem.query.all()
    assert not os.listdir(storage.root_directory)


def test_legals_acceptance_required():
    """Test if the acceptance of the legal notices is determined correctly."""
    assert not legals_acceptance_required()

    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    assert not legals_acceptance_required()

    set_sys_config(const.SYS_CONFIG_ENFORCE_LEGALS, True)
    assert legals_acceptance_required()

    remove_sys_config(const.SYS_CONFIG_TERMS_OF_USE)
    assert not legals_acceptance_required()


def test_get_legals_modification_date():
    """Test if the modification date of the legal notices is determined correctly."""
    assert get_legals_modification_date() is None

    config_item = set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    assert get_legals_modification_date() == config_item.last_modified

    config_item = set_sys_config(const.SYS_CONFIG_PRIVACY_POLICY, "Test")
    assert get_legals_modification_date() == config_item.last_modified
