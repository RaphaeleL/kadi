# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import get_flashed_messages

import kadi.lib.constants as const
from kadi.lib.config.core import get_sys_config
from kadi.lib.web import url_for
from kadi.modules.accounts.models import LocalIdentity
from tests.utils import check_view_response


def test_view_information(client, dummy_user, user_session):
    """Test the "sysadmin.view_information" endpoint."""
    dummy_user.is_sysadmin = True

    with user_session():
        response = client.get(url_for("sysadmin.view_information"))
        check_view_response(response)


def test_manage_config_customization(client, dummy_user, user_session):
    """Test the "customization" tab of the "sysadmin.manage_config" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_config", tab="customization")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"broadcast_message": "Test"})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_sys_config(const.SYS_CONFIG_BROADCAST_MESSAGE, use_fallback=False)
            == "Test"
        )


def test_manage_config_legals(client, dummy_user, user_session):
    """Test the "legals" tab of the "sysadmin.manage_config" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_config", tab="legals")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"terms_of_use": "Test"})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_sys_config(const.SYS_CONFIG_TERMS_OF_USE, use_fallback=False) == "Test"
        )


def test_manage_config_misc(client, dummy_user, user_session):
    """Test the "misc" tab of the "sysadmin.manage_config" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_config", tab="misc")

    with user_session():
        response = client.post(
            url_for("sysadmin.manage_config", tab="misc", action="test_email")
        )

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert "A test email has been sent." in get_flashed_messages()

        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"robots_noindex": "true"})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_sys_config(const.SYS_CONFIG_ROBOTS_NOINDEX, use_fallback=False) is True
        )


def test_manage_users(client, dummy_user, user_session):
    """Test the "sysadmin.manage_users" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_users")
    new_username = "test"

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "username": new_username,
                "displayname": new_username,
                "email": "test@test.com",
            },
        )

        check_view_response(response)
        assert LocalIdentity.query.filter_by(username=new_username).one()
