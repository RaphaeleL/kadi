# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.modules.templates.models import TemplateType
from kadi.modules.templates.schemas import DEFAULT_RECORD_DATA
from kadi.modules.templates.schemas import TemplateSchema


@pytest.mark.parametrize(
    "data,result",
    [
        ({}, None),
        (
            {"title": " ", "identifier": " "},
            {"title": "", "identifier": ""},
        ),
        (
            {"tags": [" B ", "a", "c   d", "A"]},
            {"tags": ["a", "b", "c d"]},
        ),
        (
            {
                "title": "test",
                "identifier": "test",
                "type": "test",
                "description": "test",
                # Use the name of the dummy license.
                "license": "test",
                "tags": ["test"],
                "extras": [{"type": "str", "key": "test", "value": None}],
                "collections": [123],
                "roles": [
                    {"subject_type": "user", "subject_id": 123, "role": "member"}
                ],
            },
            None,
        ),
    ],
)
def test_template_schema_record_data(data, result, dummy_license):
    """Test if loading record data in the "TemplateSchema" works correctly."""
    schema = TemplateSchema(only=["data"], template_type=TemplateType.RECORD)

    if result is None:
        result = data

    assert schema.load({"data": data})["data"] == {**DEFAULT_RECORD_DATA, **result}


@pytest.mark.parametrize("data", [[], [{"type": "str", "key": "test", "value": None}]])
def test_template_schema_extras_data(data):
    """Test if loading extras data in the "TemplateSchema" works correctly."""
    schema = TemplateSchema(only=["data"], template_type=TemplateType.EXTRAS)
    assert schema.load({"data": data})["data"] == data
