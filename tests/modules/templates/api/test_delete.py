# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.templates.models import TemplateState
from tests.modules.utils import check_api_delete_subject_resource_role
from tests.utils import check_api_response


def test_delete_template(api_client, dummy_personal_token, dummy_template):
    """Test the "api.delete_template" endpoint."""
    response = api_client(dummy_personal_token).delete(
        url_for("api.delete_template", id=dummy_template.id)
    )

    check_api_response(response, status_code=204)
    assert dummy_template.state == TemplateState.DELETED


def test_remove_template_user_role(
    api_client, db, dummy_personal_token, dummy_template, dummy_user, new_user
):
    """Test the "api.remove_template_user_role" endpoint."""
    user = new_user()
    client = api_client(dummy_personal_token)
    endpoint = url_for(
        "api.remove_template_user_role",
        template_id=dummy_template.id,
        user_id=user.id,
    )
    remove_creator_endpoint = url_for(
        "api.remove_template_user_role",
        template_id=dummy_template.id,
        user_id=dummy_user.id,
    )

    check_api_delete_subject_resource_role(
        db,
        client,
        endpoint,
        user,
        dummy_template,
        remove_creator_endpoint=remove_creator_endpoint,
    )


def test_remove_template_group_role(
    api_client, db, dummy_group, dummy_personal_token, dummy_template
):
    """Test the "api.remove_template_group_role" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for(
        "api.remove_template_group_role",
        template_id=dummy_template.id,
        group_id=dummy_group.id,
    )

    check_api_delete_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_template
    )
