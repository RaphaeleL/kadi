# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import functools
import os
import zipfile
from io import BytesIO

import pytest

import kadi.lib.constants as const
from kadi.lib.storage.local import create_default_local_storage
from kadi.modules.records.files import delete_file
from kadi.modules.records.files import get_custom_mimetype
from kadi.modules.records.files import get_direct_upload_type
from kadi.modules.records.files import get_permitted_files
from kadi.modules.records.files import open_file
from kadi.modules.records.files import remove_file
from kadi.modules.records.files import remove_temporary_file
from kadi.modules.records.files import stream_files
from kadi.modules.records.files import update_file
from kadi.modules.records.models import File
from kadi.modules.records.models import FileState
from kadi.modules.records.models import TemporaryFile
from kadi.modules.records.models import Upload
from kadi.modules.records.models import UploadState
from kadi.modules.records.uploads import save_chunk


def test_update_file(dummy_file, dummy_record, dummy_user):
    """Test if updating files works correctly."""
    prev_timestamp = dummy_record.last_modified

    update_file(dummy_file, name="test.txt", user=dummy_user)

    assert dummy_file.name == "test.txt"
    assert dummy_file.revisions.count() == 2
    assert dummy_record.last_modified != prev_timestamp


def test_delete_file(dummy_file, dummy_record, dummy_user, new_upload):
    """Test if deleting files works correctly."""
    prev_timestamp = dummy_record.last_modified
    upload = new_upload(file=dummy_file)

    delete_file(dummy_file, user=dummy_user)

    assert dummy_file.state == FileState.INACTIVE
    assert dummy_file.revisions.count() == 2
    assert dummy_record.last_modified != prev_timestamp
    assert upload.state == UploadState.INACTIVE


def test_remove_file(new_file, new_upload):
    """Test if removing files works correctly."""
    file_data = 10 * b"x"

    file = new_file()
    upload = new_upload(file=file, size=len(file_data))
    save_chunk(
        upload=upload, file_object=BytesIO(file_data), index=0, size=len(file_data)
    )
    remove_file(file, delete_from_db=False)

    assert file.state == FileState.DELETED
    assert file.revisions.count() == 1
    assert not Upload.query.all()
    assert not os.listdir(file.storage.root_directory)

    remove_file(file, delete_from_db=True)

    assert not File.query.all()


def test_open_file(dummy_file, dummy_image):
    """Test if opening files works correctly."""
    with open_file(dummy_file) as f:
        assert f.read() == dummy_image.getvalue()

    assert f.closed


def test_stream_files(dummy_file, dummy_record):
    """Test if streaming multiple files as a ZIP archive works correctly."""
    response_stream = stream_files(dummy_record)
    data = functools.reduce(lambda acc, val: acc + val, response_stream.response)

    # pylint: disable=consider-using-with
    zip_file = zipfile.ZipFile(BytesIO(data))
    zip_item = zip_file.infolist()[0]

    assert zip_item.filename == dummy_file.name
    assert zip_item.file_size == dummy_file.size


@pytest.mark.parametrize(
    "file_data,mimetype",
    [
        (
            b"""
            {
                "nodes": [],
                "connections": []
            }
            """,
            const.MIMETYPE_FLOW,
        ),
        (
            b"""
            <program name="test">
                <param name="test" type="test"></param>
            </program>
            """,
            const.MIMETYPE_TOOL,
        ),
        (
            b"""
            {
                "name": "dashboard",
                "panels": []
            }
            """,
            const.MIMETYPE_DASHBOARD,
        ),
        (
            b"""
            <foo name="bar">
                <baz></baz>
            </foo>
            """,
            None,
        ),
        (
            b"""
            {
                "foo": [],
                "bar": []
            }
            """,
            None,
        ),
    ],
)
def test_get_custom_mimetype(file_data, mimetype, new_file):
    """Test if determining custom MIME types works correctly.

    Based on the built-in custom MIME types.
    """
    file = new_file(file_data=file_data)
    custom_mimetype = get_custom_mimetype(file)

    if mimetype is None:
        assert custom_mimetype is None
    else:
        assert custom_mimetype == mimetype


def test_get_direct_upload_type(new_file):
    """Test if direct upload types are determined correctly."""
    file = new_file(file_data=b"test")
    assert get_direct_upload_type(file) == "text"


def test_get_permitted_files(dummy_file, dummy_record, dummy_user, new_user):
    """Test if permitted files are determined correctly."""
    user = new_user()

    assert (
        get_permitted_files(filter_term=dummy_file.name, user=dummy_user).one()
        == dummy_file
    )
    assert (
        get_permitted_files(record_id=dummy_record.id, user=dummy_user).one()
        == dummy_file
    )
    assert not get_permitted_files(user=user).all()


def test_remove_temporary_file(dummy_temporary_file):
    """Test if removing temporary files works correctly."""
    remove_temporary_file(dummy_temporary_file)

    assert not TemporaryFile.query.all()

    storage = create_default_local_storage()

    assert not os.listdir(storage.root_directory)
