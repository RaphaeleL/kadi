# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.modules.records.files import delete_file
from kadi.modules.records.models import File
from kadi.modules.records.models import FileState
from kadi.modules.records.models import TemporaryFile
from kadi.modules.records.models import Upload
from kadi.modules.records.uploads import delete_upload
from kadi.modules.records.utils import clean_files


def test_clean_files(
    monkeypatch,
    dummy_file,
    dummy_record,
    dummy_temporary_file,
    dummy_upload,
    dummy_user,
    new_upload,
):
    """Test if cleaning files works correctly."""

    # To simulate an expired upload.
    new_upload()
    # To simulate an inactive upload.
    delete_upload(dummy_upload)
    # To simulate an inactive file.
    delete_file(dummy_file, user=dummy_user)

    clean_files()

    assert Upload.query.count() == 2
    assert File.query.one()
    assert dummy_file.state == FileState.INACTIVE
    assert TemporaryFile.query.one()

    monkeypatch.setitem(current_app.config, "UPLOADS_MAX_AGE", 0)
    monkeypatch.setitem(current_app.config, "TEMPORARY_FILES_MAX_AGE", 0)
    monkeypatch.setitem(current_app.config, "INACTIVE_FILES_MAX_AGE", 0)

    clean_files()

    # The inactive upload should still remain.
    assert Upload.query.one() == dummy_upload
    assert File.query.one()
    assert dummy_file.state == FileState.DELETED
    assert not TemporaryFile.query.all()
