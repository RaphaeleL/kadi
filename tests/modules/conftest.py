# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask_login import current_user

import kadi.lib.constants as const
from kadi.lib.permissions.core import apply_role_rule
from kadi.lib.permissions.models import RoleRule
from kadi.lib.tasks.models import Task
from kadi.lib.tasks.models import TaskState
from kadi.lib.utils import named_tuple
from kadi.modules.accounts.providers.ldap import LDAPProvider
from kadi.modules.accounts.providers.shib import ShibProvider
from kadi.modules.records.core import purge_record
from kadi.modules.records.uploads import merge_chunks


@pytest.fixture(autouse=True)
def _auth_providers(monkeypatch):
    """Fixture to patch all authentication providers relying on external systems.

    Will patch the providers in all relevant modules where they are used.

    Executed automatically for each test.
    """

    class _LDAPProvider(LDAPProvider):
        @classmethod
        def authenticate(cls, *, username, password, **kwargs):
            ldap_info = named_tuple("LDAPInfo", username="", email="", displayname="")
            return cls.UserInfo(True, ldap_info)

    class _ShibProvider(ShibProvider):
        @classmethod
        def contains_valid_idp(cls):
            return True

        @classmethod
        def change_password(cls, username, old_password, new_password):
            raise NotImplementedError

        @classmethod
        def authenticate(cls, **kwargs):
            shib_info = named_tuple("ShibInfo", username="", email="", displayname="")
            return cls.UserInfo(True, shib_info)

    monkeypatch.setattr("kadi.modules.accounts.views.LDAPProvider", _LDAPProvider)
    monkeypatch.setattr("kadi.modules.accounts.views.ShibProvider", _ShibProvider)


@pytest.fixture(autouse=True)
def _send_mail_task(monkeypatch):
    """Fixture to patch the "send_mail" background task.

    Simulates the result the actual task would produce if an email would have been sent
    successfully. The function to launch the task is patched in all relevant modules
    where it is used.

    Executed automatically for each test.
    """

    def _start_send_mail_task(*args, **kwargs):
        return True

    for module in [
        "kadi.modules.accounts.views.send_email_confirmation_mail",
        "kadi.modules.accounts.views.send_password_reset_mail",
        "kadi.modules.settings.views.send_email_confirmation_mail",
        "kadi.modules.sysadmin.views.send_test_mail",
    ]:
        monkeypatch.setattr(module, _start_send_mail_task)


@pytest.fixture(autouse=True)
def _apply_role_rules_task(monkeypatch, db):
    """Fixture to patch the "apply_role_rules" background task.

    Simulates the result the actual task would produce. The function to launch the task
    is patched in all relevant modules where it is used.

    Executed automatically for each test.
    """

    def _start_apply_role_rules_task(role_rule=None, user=None):
        if role_rule is not None:
            apply_role_rule(role_rule, user=user)
            db.session.commit()
        else:
            for _role_rule in RoleRule.query.order_by(RoleRule.created_at):
                apply_role_rule(_role_rule, user=user)
                db.session.commit()

        return True

    monkeypatch.setattr(
        "kadi.modules.accounts.providers.core.start_apply_role_rules_task",
        _start_apply_role_rules_task,
    )
    monkeypatch.setattr(
        "kadi.modules.groups.views.start_apply_role_rules_task",
        _start_apply_role_rules_task,
    )


@pytest.fixture(autouse=True)
def _purge_record_task(monkeypatch):
    """Fixture to patch the "purge_record" background task.

    Simulates the result the actual task would produce. The function to launch the task
    is patched in all relevant modules where it is used.

    Executed automatically for each test.
    """

    def _start_purge_record_task(record):
        purge_record(record)
        return True

    monkeypatch.setattr(
        "kadi.modules.records.api.public.post.start_purge_record_task",
        _start_purge_record_task,
    )


@pytest.fixture(autouse=True)
def _merge_chunks_task(monkeypatch, db):
    """Fixture to patch the "merge_chunks" background task.

    Simulates the result the actual task would produce. The function to launch the task
    is patched in all relevant modules where it is used.

    Executed automatically for each test.
    """

    def _start_merge_chunks_task(upload, user=None):
        user = user if user is not None else current_user

        task = Task.create(
            name=const.TASK_MERGE_CHUNKS,
            creator=user,
            args=[str(upload.id)],
            state=TaskState.RUNNING,
        )

        file = merge_chunks(upload, task=task)
        task.result = {"file": str(file.id)}

        task.state = TaskState.SUCCESS
        db.session.commit()

        return task

    monkeypatch.setattr(
        "kadi.modules.records.api.public.post.start_merge_chunks_task",
        _start_merge_chunks_task,
    )
