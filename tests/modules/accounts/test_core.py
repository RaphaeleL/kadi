# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.models import Role
from kadi.modules.accounts.core import merge_users
from kadi.modules.accounts.core import purge_user
from kadi.modules.accounts.models import User


def test_purge_user(
    dummy_collection,
    dummy_file,
    dummy_group,
    dummy_oauth2_server_client,
    dummy_personal_token,
    dummy_record,
    dummy_template,
    dummy_temporary_file,
    dummy_upload,
    dummy_user,
):
    """Test if purging users works correctly."""
    purge_user(dummy_user)
    assert not User.query.all()


def test_merge_users(dummy_record, dummy_user, new_user):
    """Test if merging multiple users works correctly."""
    new_username = "test"
    user = new_user(username=new_username)
    merge_users(user, dummy_user)

    assert dummy_user.new_user_id == user.id
    assert dummy_user.is_merged
    assert not dummy_user.identity
    assert not dummy_user.identities.all()
    assert not dummy_user.records.all()
    assert not dummy_user.roles.all()

    assert not user.is_merged
    assert user.identity.username == new_username
    assert user.identities.count() == 2
    assert user.records.one() == dummy_record
    assert user.roles.filter(
        Role.object == "record", Role.object_id == dummy_record.id
    ).one()
