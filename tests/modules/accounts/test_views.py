# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app
from flask import get_flashed_messages
from flask_login import current_user

import kadi.lib.constants as const
from kadi.lib.config.core import set_sys_config
from kadi.lib.web import get_next_url
from kadi.lib.web import url_for
from kadi.modules.accounts.models import LocalIdentity
from kadi.modules.accounts.models import UserState
from tests.utils import check_view_response


def test_login(client):
    """Test the "accounts.login" endpoint."""
    response = client.get(url_for("accounts.login"))
    check_view_response(response)


def test_login_with_provider_local(client, dummy_user, request_context):
    """Test the "accounts.login_with_provider" endpoint with the local provider."""
    provider = const.AUTH_PROVIDER_TYPE_LOCAL

    endpoint = url_for("accounts.login_with_provider", provider=provider)
    next_url = get_next_url()

    response = client.get(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == url_for("accounts.login", tab=provider)

    with client:
        # The current user is only available here because of the dummy request context.
        # Otherwise, a request would need to be sent first.
        assert not current_user.is_authenticated

        username = password = dummy_user.identity.username
        response = client.post(
            endpoint, data={"username": username, "password": password}
        )

        check_view_response(response, status_code=302)
        assert response.location == next_url
        assert current_user.is_authenticated


def test_login_with_provider_ldap(client, request_context):
    """Test the "accounts.login_with_provider" endpoint with the LDAP provider."""
    provider = const.AUTH_PROVIDER_TYPE_LDAP

    endpoint = url_for("accounts.login_with_provider", provider=provider)
    next_url = get_next_url()

    response = client.get(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == url_for("accounts.login", tab=provider)

    with client:
        assert not current_user.is_authenticated

        # The actual data posted here does not matter (as long as it is not empty), as
        # the provider is mocked.
        response = client.post(endpoint, data={"username": "test", "password": "test"})

        check_view_response(response, status_code=302)
        assert response.location == next_url
        assert current_user.is_authenticated


def test_login_with_provider_shib(client, request_context):
    """Test the "accounts.login_with_provider" endpoint with the Shibboleth provider."""
    provider = const.AUTH_PROVIDER_TYPE_SHIB

    idp = current_app.config["AUTH_PROVIDERS"][provider]["idps"][0]["entity_id"]
    endpoint = url_for("accounts.login_with_provider", provider=provider)
    next_url = get_next_url()

    response = client.post(endpoint, data={"idp": idp})

    check_view_response(response, status_code=302)
    assert idp in response.location
    assert (
        url_for("accounts.login_with_provider", provider=provider, next=next_url)
        in response.location
    )

    with client:
        assert not current_user.is_authenticated

        response = client.get(endpoint)

        check_view_response(response, status_code=302)
        assert response.location == next_url
        assert current_user.is_authenticated


def test_logout(client, user_session):
    """Test the "accounts.logout" endpoint."""
    with user_session():
        assert current_user.is_authenticated

        client.get(url_for("accounts.logout"))

        assert not current_user.is_authenticated


@pytest.mark.parametrize("enforce_legals", [True, False])
@pytest.mark.parametrize("accept_legals", [True, False])
def test_register(monkeypatch, enforce_legals, accept_legals, client):
    """Test the "accounts.register" endpoint."""
    monkeypatch.setitem(
        current_app.config["AUTH_PROVIDERS"][const.AUTH_PROVIDER_TYPE_LOCAL],
        "email_confirmation_required",
        True,
    )
    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    set_sys_config(const.SYS_CONFIG_ENFORCE_LEGALS, enforce_legals)

    username = "test"
    flash_msg = "A confirmation email has been sent."
    endpoint = url_for("accounts.register")

    response = client.get(endpoint)
    check_view_response(response)

    with client:
        response = client.post(
            endpoint,
            data={
                "username": username,
                "displayname": username,
                "email": "test@example.com",
                "password": "test1234",
                "password2": "test1234",
                "accept_legals": "true" if accept_legals else "false",
            },
        )

        if enforce_legals:
            if accept_legals:
                local_identity = LocalIdentity.query.filter_by(username=username).one()

                check_view_response(response, status_code=302)
                assert flash_msg in get_flashed_messages()
                assert local_identity.user.legals_accepted
            else:
                check_view_response(response)
                assert not LocalIdentity.query.filter_by(username=username).first()
        else:
            local_identity = LocalIdentity.query.filter_by(username=username).one()

            check_view_response(response, status_code=302)
            assert flash_msg in get_flashed_messages()
            assert not local_identity.user.legals_accepted


def test_request_password_reset(client, dummy_user):
    """Test the "accounts.request_password_reset" endpoint."""
    endpoint = url_for("accounts.request_password_reset")

    response = client.get(endpoint)
    check_view_response(response)

    with client:
        response = client.post(
            endpoint, data={"username": dummy_user.identity.username}
        )

        check_view_response(response, status_code=302)
        assert response.location == url_for(
            "accounts.login", tab=const.AUTH_PROVIDER_TYPE_LOCAL
        )
        assert "A password reset email has been sent." in get_flashed_messages()


def test_reset_password(client, dummy_user):
    """Test the "accounts.reset_password" endpoint."""
    token = dummy_user.identity.get_password_reset_token()

    response = client.get(url_for("accounts.reset_password", token=token))
    check_view_response(response)

    response = client.post(
        url_for("accounts.reset_password", token=token),
        data={"password": "test1234", "password2": "test1234"},
    )

    check_view_response(response, status_code=302)
    assert response.location == url_for(
        "accounts.login", tab=const.AUTH_PROVIDER_TYPE_LOCAL
    )
    assert dummy_user.identity.check_password("test1234")


def test_request_email_confirmation(monkeypatch, client, user_session):
    """Test the "accounts.request_email_confirmation" endpoint."""
    monkeypatch.setitem(
        current_app.config["AUTH_PROVIDERS"][const.AUTH_PROVIDER_TYPE_LOCAL],
        "email_confirmation_required",
        True,
    )

    endpoint = url_for("accounts.request_email_confirmation")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint)

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert "A confirmation email has been sent." in get_flashed_messages()


@pytest.mark.parametrize("email", ["test@example.com", None])
def test_confirm_email(monkeypatch, email, client, dummy_user):
    """Test the "accounts.confirm_email" endpoint."""
    monkeypatch.setitem(
        current_app.config["AUTH_PROVIDERS"][const.AUTH_PROVIDER_TYPE_LOCAL],
        "email_confirmation_required",
        True,
    )

    token = dummy_user.identity.get_email_confirmation_token(email=email)

    assert not dummy_user.identity.email_confirmed

    prev_email = dummy_user.identity.email
    client.get(url_for("accounts.confirm_email", token=token))

    assert dummy_user.identity.email_confirmed

    if email is not None:
        assert dummy_user.identity.email == email
    else:
        assert dummy_user.identity.email == prev_email


@pytest.mark.parametrize("enforce_legals", [True, False])
def test_request_legals_acceptance(enforce_legals, client, dummy_user, user_session):
    """Test the "accounts.request_legals_acceptance" endpoint."""
    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    set_sys_config(const.SYS_CONFIG_ENFORCE_LEGALS, enforce_legals)

    endpoint = url_for("accounts.request_legals_acceptance")

    assert not dummy_user.legals_accepted

    with user_session():
        response = client.get(endpoint)

        if not enforce_legals:
            check_view_response(response, status_code=302)
        else:
            check_view_response(response)

            response = client.post(endpoint, data={"accept_legals": True})

            check_view_response(response, status_code=302)
            assert dummy_user.legals_accepted


def test_inactive_user(client, dummy_user, user_session):
    """Test the "accounts.inactive_user" endpoint."""
    dummy_user.state = UserState.DELETED

    with user_session():
        response = client.get(url_for("accounts.inactive_user"))
        check_view_response(response)


def test_users(client, user_session):
    """Test the "accounts.users" endpoint."""
    with user_session():
        response = client.get(url_for("accounts.users"))
        check_view_response(response)


def test_view_user(client, dummy_user, user_session):
    """Test the "accounts.view_user" endpoint."""
    with user_session():
        response = client.get(url_for("accounts.view_user", id=dummy_user.id))
        check_view_response(response)


def test_view_resources(client, dummy_user, user_session):
    """Test the "accounts.view_resources" endpoint."""
    with user_session():
        response = client.get(url_for("accounts.view_resources", id=dummy_user.id))
        check_view_response(response)


def test_manage_trash(client, user_session):
    """Test the "accounts.manage_trash" endpoint."""
    with user_session():
        response = client.get(url_for("accounts.manage_trash"))
        check_view_response(response)
