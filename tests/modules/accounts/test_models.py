# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import kadi.lib.constants as const
from kadi.lib.config.core import set_sys_config


def test_user_needs_legals_acceptance(dummy_user):
    """Test if the "needs_legals_acceptance" property of users works correctly."""
    assert not dummy_user.needs_legals_acceptance

    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    set_sys_config(const.SYS_CONFIG_ENFORCE_LEGALS, True)

    assert dummy_user.needs_legals_acceptance

    dummy_user.accept_legals()
    assert not dummy_user.needs_legals_acceptance

    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test2")
    assert dummy_user.needs_legals_acceptance
