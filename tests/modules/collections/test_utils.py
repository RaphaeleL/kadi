# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.core import add_role
from kadi.lib.resources.utils import add_link
from kadi.modules.collections.core import link_collections
from kadi.modules.collections.utils import get_child_collection_records
from kadi.modules.collections.utils import get_child_collections
from kadi.modules.collections.utils import get_collection_links_graph
from kadi.modules.collections.utils import get_parent_collections
from kadi.modules.records.links import create_record_link


def test_get_parent_collections(dummy_user, new_collection, new_user):
    """Test if parents of collections are determined correctly."""
    user = new_user()
    collection_1 = new_collection()
    collection_2 = new_collection()
    collection_3 = new_collection()

    link_collections(collection_1, collection_2, user=dummy_user)
    link_collections(collection_2, collection_3, user=dummy_user)

    assert get_parent_collections(collection_3, user=dummy_user) == [
        collection_2,
        collection_1,
    ]
    assert not get_parent_collections(collection_3, user=user)

    # Give the new user access to the intermediate collection.
    add_role(user, "collection", collection_2.id, "member")
    assert get_parent_collections(collection_3, user=user) == [collection_2]


def test_get_child_collections(dummy_user, new_collection, new_user):
    """Test if children of collections are determined correctly."""
    user = new_user()
    collection_1 = new_collection()
    collection_2 = new_collection()
    collection_3 = new_collection()

    link_collections(collection_1, collection_2, user=dummy_user)
    link_collections(collection_2, collection_3, user=dummy_user)

    children = get_child_collections(collection_1, user=dummy_user)

    assert len(children) == 2
    assert collection_2 in children
    assert collection_3 in children

    assert not get_child_collections(collection_1, user=user)

    # Give the new user access to the intermediate collection.
    add_role(user, "collection", collection_2.id, "member")
    assert get_child_collections(collection_1, user=user) == [collection_2]


def test_get_child_collection_records(dummy_user, new_collection, new_record, new_user):
    """Test if records of collection hierarchies are determined correctly."""
    user = new_user()
    collection_1 = new_collection()
    collection_2 = new_collection()
    collection_3 = new_collection()
    record_1 = new_record()
    record_2 = new_record()

    link_collections(collection_1, collection_2, user=dummy_user)
    link_collections(collection_2, collection_3, user=dummy_user)
    add_link(collection_1.records, record_1, user=dummy_user)
    add_link(collection_2.records, record_2, user=dummy_user)
    add_link(collection_3.records, record_2, user=dummy_user)

    records = get_child_collection_records(collection_1, user=dummy_user).all()

    assert len(records) == 2
    assert record_1 in records
    assert record_2 in records

    assert not get_child_collection_records(collection_1, user=user).all()

    # Give the new user access to the records only.
    add_role(user, "record", record_1.id, "member")
    add_role(user, "record", record_2.id, "member")

    assert get_child_collection_records(collection_1, user=user).one() == record_1


def test_get_collection_links_graph(
    dummy_collection, dummy_user, new_collection, new_record
):
    """Test if collection link graph data is generated correctly."""
    record_1 = new_record()
    record_2 = new_record()
    collection = new_collection()

    record_link = create_record_link(
        name="test", record_from=record_1, record_to=record_2, creator=dummy_user
    )

    link_collections(dummy_collection, collection, user=dummy_user)
    add_link(dummy_collection.records, record_1, user=dummy_user)
    add_link(dummy_collection.records, record_2, user=dummy_user)

    data = get_collection_links_graph(
        dummy_collection, records=True, children=True, user=dummy_user
    )

    assert data["id"] == dummy_collection.id

    children = data["children"]

    # pylint: disable=unsubscriptable-object
    assert len(children) == 1
    assert children[0]["id"] == collection.id

    records = data["records"]

    assert len(records) == 2
    assert records[0]["id"] == f"{dummy_collection.id}-{record_2.id}"
    assert records[1]["id"] == f"{dummy_collection.id}-{record_1.id}"

    record_links = data["record_links"]

    assert len(record_links) == 1
    assert record_links[0]["id"] == f"{dummy_collection.id}-{record_link.id}"
