# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

import kadi.lib.constants as const
from kadi.lib.web import url_for
from tests.utils import check_api_response


def _get_export_mimetype(export_type, download=True):
    if export_type == "qr" and download:
        return const.MIMETYPE_PNG

    if export_type == "rdf":
        return f"{const.MIMETYPE_TTL}; charset=utf-8"

    if export_type == "ro-crate":
        if download:
            return const.MIMETYPE_ZIP

        return const.MIMETYPE_JSONLD

    return const.MIMETYPE_JSON


@pytest.mark.parametrize("export_type", const.EXPORT_TYPES["collection"])
@pytest.mark.parametrize("download", [True, False])
def test_get_collection_export_internal(
    export_type, download, client, dummy_collection, user_session
):
    """Test the internal "api.get_collection_export_internal" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.get_collection_export_internal",
                id=dummy_collection.id,
                export_type=export_type,
                download=download,
            )
        )

        content_type = _get_export_mimetype(export_type, download)

        check_api_response(response, content_type=content_type)
        response.close()


def test_get_collection_links_graph(client, dummy_collection, user_session):
    """Test the internal "api.get_collection_links_graph" endpoint."""
    with user_session():
        response = client.get(
            url_for("api.get_collection_links_graph", id=dummy_collection.id)
        )
        check_api_response(response)


def test_select_collections(client, user_session):
    """Test the internal "api.select_collections" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_collections"))
        check_api_response(response)


def test_get_collections(api_client, dummy_personal_token):
    """Test the "api.get_collections" endpoint."""
    response = api_client(dummy_personal_token).get(url_for("api.get_collections"))
    check_api_response(response)


def test_get_collection_by_identifier(
    api_client, dummy_collection, dummy_personal_token
):
    """Test the "api.get_collection_by_identifier" endpoint."""
    response = api_client(dummy_personal_token).get(
        url_for(
            "api.get_collection_by_identifier", identifier=dummy_collection.identifier
        )
    )
    check_api_response(response)


def test_get_collection_revision(api_client, dummy_collection, dummy_personal_token):
    """Test the "api.get_collection_revision" endpoint."""
    response = api_client(dummy_personal_token).get(
        url_for(
            "api.get_collection_revision",
            collection_id=dummy_collection.id,
            revision_id=dummy_collection.ordered_revisions.first().id,
        )
    )
    check_api_response(response)


@pytest.mark.parametrize("export_type", const.EXPORT_TYPES["collection"])
def test_get_collection_export(export_type, client, dummy_collection, user_session):
    """Test the "api.get_collection_export" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.get_collection_export",
                id=dummy_collection.id,
                export_type=export_type,
            )
        )
        content_type = _get_export_mimetype(export_type)

        check_api_response(response, content_type=content_type)
        response.close()


@pytest.mark.parametrize(
    "endpoint",
    [
        "get_collection",
        "get_collection_records",
        "get_child_collections",
        "get_collection_user_roles",
        "get_collection_group_roles",
        "get_collection_revisions",
    ],
)
def test_get_collection_endpoints(
    endpoint, api_client, dummy_collection, dummy_personal_token
):
    """Test the remaining "api.get_collection*" endpoints."""
    response = api_client(dummy_personal_token).get(
        url_for(f"api.{endpoint}", id=dummy_collection.id)
    )
    check_api_response(response)
