# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

import pytest
from flask import current_app

import kadi.lib.constants as const
from kadi.lib.notifications.models import Notification
from kadi.lib.search.models import SavedSearch
from kadi.lib.web import url_for
from kadi.modules.collections.core import delete_collection
from kadi.modules.groups.core import delete_group
from kadi.modules.records.core import delete_record
from kadi.modules.sysadmin.utils import save_index_image
from kadi.modules.templates.core import delete_template
from tests.utils import check_api_response


SAVED_SEARCH_PARAMS = {"name": "test", "object": "record", "query_string": "foo=bar"}


def test_new_saved_search(client, user_session):
    """Test the internal "api.new_saved_search" endpoint."""
    with user_session():
        response = client.post(
            url_for("api.new_saved_search"), json=SAVED_SEARCH_PARAMS
        )

        check_api_response(response, status_code=201)
        assert SavedSearch.query.filter_by(name="test").one()


def test_edit_saved_search(client, dummy_user, user_session):
    """Test the internal "api.edit_saved_search" endpoint."""
    saved_search = SavedSearch.create(user=dummy_user, **SAVED_SEARCH_PARAMS)

    with user_session():
        response = client.patch(
            url_for("api.edit_saved_search", id=saved_search.id), json={"name": "test2"}
        )

        check_api_response(response, status_code=200)
        assert saved_search.name == "test2"


def test_dismiss_notification(client, db, dummy_user, user_session):
    """Test the internal "api.dismiss_notification" endpoint."""
    notification = Notification.create(user=dummy_user, name="test")
    db.session.commit()

    with user_session():
        response = client.delete(
            url_for("api.dismiss_notification", id=notification.id)
        )
        db.session.commit()

        check_api_response(response, status_code=204)
        assert not Notification.query.all()


def test_remove_saved_search(client, db, dummy_user, user_session):
    """Test the internal "api.remove_saved_search" endpoint."""
    saved_search = SavedSearch.create(user=dummy_user, **SAVED_SEARCH_PARAMS)

    with user_session():
        response = client.delete(url_for("api.remove_saved_search", id=saved_search.id))
        db.session.commit()

        check_api_response(response, status_code=204)
        assert not SavedSearch.query.all()


def test_get_saved_search(client, dummy_user, user_session):
    """Test the internal "api.get_saved_search" endpoint."""
    saved_search = SavedSearch.create(user=dummy_user, **SAVED_SEARCH_PARAMS)

    with user_session():
        response = client.get(url_for("api.get_saved_search", id=saved_search.id))
        check_api_response(response, status_code=200)


def test_preview_index_image(monkeypatch, tmp_path, client, dummy_image, user_session):
    """Test the internal "api.preview_index_image" endpoint."""
    endpoint = url_for("api.preview_index_image")

    # Test with an image provided via an absolute file path.
    filepath = os.path.join(tmp_path, "index.jpg")
    monkeypatch.setitem(current_app.config, const.SYS_CONFIG_INDEX_IMAGE, filepath)

    with open(filepath, mode="wb") as f:
        f.write(dummy_image.getbuffer())

    with user_session():
        response = client.get(endpoint)
        check_api_response(response, content_type=const.MIMETYPE_JPEG)
        response.close()

    # Test with an image provided via global config item.
    save_index_image(dummy_image)

    with user_session():
        response = client.get(endpoint)
        check_api_response(response, content_type=const.MIMETYPE_JPEG)
        response.close()


def test_check_identifier(client, dummy_record, user_session):
    """Test the internal "api.check_identifier" endpoint."""
    with user_session():
        response = client.get(url_for("api.check_identifier", resource_type="test"))

        check_api_response(response, status_code=404)

        response = client.get(
            url_for(
                "api.check_identifier",
                resource_type="record",
                identifier=dummy_record.identifier,
            )
        )

        check_api_response(response)
        assert response.get_json()["duplicate"] is True

        response = client.get(
            url_for(
                "api.check_identifier",
                resource_type="record",
                identifier=dummy_record.identifier,
                exclude=dummy_record.id,
            )
        )

        check_api_response(response)
        assert response.get_json()["duplicate"] is False


@pytest.mark.parametrize(
    "endpoint",
    [
        "get_notifications",
        "quick_search",
        "select_saved_searches",
        "select_tags",
        "select_licenses",
    ],
)
def test_internal_endpoints(endpoint, client, user_session):
    """Test all remaining internal endpoints."""
    with user_session():
        response = client.get(url_for(f"api.{endpoint}"))
        check_api_response(response)


def test_get_trash(
    api_client,
    dummy_collection,
    dummy_group,
    dummy_personal_token,
    dummy_record,
    dummy_template,
    dummy_user,
):
    """Test the "api.get_trash" endpoint."""
    delete_collection(dummy_collection, user=dummy_user)
    delete_group(dummy_group, user=dummy_user)
    delete_record(dummy_record, user=dummy_user)
    delete_template(dummy_template, user=dummy_user)

    response = api_client(dummy_personal_token).get(url_for("api.get_trash"))
    resources = response.get_json()["items"]

    check_api_response(response)
    assert len(resources) == 4
    assert resources[0]["type"] == "template"
    assert resources[1]["type"] == "record"
    assert resources[2]["type"] == "group"
    assert resources[3]["type"] == "collection"


@pytest.mark.parametrize(
    "endpoint", ["index", "get_info", "get_resource_roles", "get_tags", "get_licenses"]
)
def test_public_endpoints(endpoint, api_client, dummy_personal_token):
    """Test all remaining public endpoints."""
    response = api_client(dummy_personal_token).get(url_for(f"api.{endpoint}"))
    check_api_response(response)
