# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.groups.core import delete_group
from kadi.modules.groups.models import Group
from kadi.modules.groups.models import GroupState
from tests.modules.utils import check_api_post_subject_resource_role
from tests.utils import check_api_response


def test_new_group(api_client, dummy_personal_token):
    """Test the "api.new_group" endpoint."""
    response = api_client(dummy_personal_token).post(
        url_for("api.new_group"), json={"identifier": "test", "title": "test"}
    )

    check_api_response(response, status_code=201)
    assert Group.query.filter_by(identifier="test").one()


def test_add_group_member(api_client, db, dummy_group, dummy_personal_token, new_user):
    """Test the "api.add_group_member" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for("api.add_group_member", id=dummy_group.id)

    check_api_post_subject_resource_role(db, client, endpoint, new_user(), dummy_group)


def test_restore_group(api_client, dummy_group, dummy_personal_token, dummy_user):
    """Test the "api.restore_group" endpoint."""
    delete_group(dummy_group, user=dummy_user)

    response = api_client(dummy_personal_token).post(
        url_for("api.restore_group", id=dummy_group.id)
    )

    check_api_response(response)
    assert dummy_group.state == GroupState.ACTIVE


def test_purge_group(api_client, dummy_group, dummy_personal_token, dummy_user):
    """Test the "api.purge_group" endpoint."""
    delete_group(dummy_group, user=dummy_user)

    response = api_client(dummy_personal_token).post(
        url_for("api.purge_group", id=dummy_group.id)
    )

    check_api_response(response, status_code=204)
    assert Group.query.get(dummy_group.id) is None
