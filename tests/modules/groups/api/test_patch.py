# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from tests.modules.utils import check_api_patch_subject_resource_role
from tests.utils import check_api_response


def test_toggle_favorite_group(client, dummy_group, dummy_user, user_session):
    """Test the internal "api.toggle_favorite_group" endpoint."""
    with user_session():
        response = client.patch(url_for("api.toggle_favorite_group", id=dummy_group.id))

        check_api_response(response, status_code=204)
        assert dummy_group.is_favorite(user=dummy_user)


def test_edit_group(api_client, dummy_group, dummy_personal_token):
    """Test the "api.edit_group" endpoint."""
    response = api_client(dummy_personal_token).patch(
        url_for("api.edit_group", id=dummy_group.id), json={"identifier": "test"}
    )

    check_api_response(response)
    assert dummy_group.identifier == "test"


def test_change_group_member(
    api_client, db, dummy_group, dummy_personal_token, dummy_user, new_user
):
    """Test the "api.change_group_member" endpoint."""
    user = new_user()
    client = api_client(dummy_personal_token)
    endpoint = url_for(
        "api.change_group_member", group_id=dummy_group.id, user_id=user.id
    )
    change_creator_endpoint = url_for(
        "api.change_group_member", group_id=dummy_group.id, user_id=dummy_user.id
    )

    check_api_patch_subject_resource_role(
        db,
        client,
        endpoint,
        user,
        dummy_group,
        change_creator_endpoint=change_creator_endpoint,
    )
