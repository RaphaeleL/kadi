# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.ldap import make_upn


@pytest.mark.parametrize(
    "username,dn,result",
    [
        ("user", "ou=test,DC=kit,dc=edu", "user@kit.edu"),
        ("user", "ou=test", None),
        ("user", "", None),
    ],
)
def test_make_upn(username, dn, result):
    """Test if creating UPNs for Active Directories works correctly."""
    assert make_upn(username, dn) == result
