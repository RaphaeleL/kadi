# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.api.utils import get_api_version
from kadi.lib.api.utils import is_api_request
from kadi.lib.api.utils import is_internal_api_request
from kadi.lib.web import url_for
from tests.utils import check_api_response


@pytest.mark.parametrize(
    "endpoint,result",
    [("main.index", False), ("api.index", True), ("api.index_v1", True)],
)
def test_is_api_request(endpoint, result, client, user_session):
    """Test if API requests are detected correctly."""
    with user_session():
        client.get(url_for(endpoint))
        assert is_api_request() is result


@pytest.mark.parametrize(
    "endpoint,internal,result",
    [
        ("api.index", False, False),
        ("api.index", True, True),
        ("main.index", False, False),
        ("main.index", True, False),
    ],
)
def test_is_internal_api_request(endpoint, internal, result, client, user_session):
    """Test if internal API requests are detected correctly."""
    with user_session():
        values = {"_internal": True} if internal else {}
        client.get(url_for(endpoint, **values))

        assert is_internal_api_request() is result


@pytest.mark.parametrize(
    "endpoint,version",
    [("api.index", None), ("main.index", None), ("api.index_v1", "v1")],
)
def test_get_api_version(endpoint, version, client, user_session):
    """Test if API versions are determined correctly."""
    with user_session():
        client.get(url_for(endpoint))
        assert get_api_version() == version


def test_check_max_content_length(monkeypatch, api_client, dummy_personal_token):
    """Check if the maximum content length is handled correctly."""
    monkeypatch.setitem(current_app.config, "MAX_CONTENT_LENGTH", 1)

    response = api_client(dummy_personal_token).post(
        url_for("api.new_record"), json="test"
    )
    check_api_response(response, status_code=413)
